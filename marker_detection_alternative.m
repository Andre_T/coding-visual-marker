%% marker_detection_alternative
%localizza i marker in base alle barre-guida, trovando i quattro punti che
%identificano il marker


function marker_detection_alternative(im)

im = imread(im);

%complemento dell'immagine binaria cos� da avere le barre-guida bianche
bw = ~im2bw(im);
cc = bwconncomp(bw);
stat = regionprops(cc, {'Area', 'Centroid', 'Orientation' ...
    'MajorAxisLength', 'MinorAxisLength', 'Image', ...
    'BoundingBox', 'Eccentricity'});

%classifica le regioni in base alle caratteristiche spaziali. In
%particolare seleziona le barre-guida
idx = find([stat.MajorAxisLength] ./ [stat.MinorAxisLength] > 3 & ...
    [stat.MajorAxisLength] ./ [stat.MinorAxisLength] < 12 & ...
    [stat.Eccentricity] >= 0.96 & [stat.Eccentricity] <= 1.0);

%elimina i falsi positivi
am = min(stat(idx).Area);
gbm = find(am == [stat.Area]);
aM = max(stat(idx).Area);
gbM = find(aM == [stat.Area]);

bw2 = ismember(labelmatrix(cc), idx);

%calcola le distanze tra il centro e gli assi dell'ellisse
dxM = (((stat(gbM).MajorAxisLength / 2) + stat(gbM).MinorAxisLength) + cos(stat(gbM).Orientation));
dyM = (((stat(gbM).MajorAxisLength / 2) + stat(gbM).MinorAxisLength) + sin(stat(gbM).Orientation));
dxm = (((stat(gbm).MajorAxisLength / 2) * 3) + cos(stat(gbm).Orientation));
dym = (((stat(gbm).MajorAxisLength / 2) * 3) + sin(stat(gbm).Orientation));

c1 = [];
c2 = [];
figure, imshow(bw2), hold on;
%salva i centroidi delle regioni
for k = 1 : numel(stat)
    %text(stat(k).Centroid(1), stat(k).Centroid(2), ...
     %   sprintf('%4.3f', stat(k).Centroid(1)), ...
      %  'EdgeColor', 'b', 'Color', 'r');
    %rectangle('Position', stat(k).BoundingBox, 'EdgeColor', 'r');
    c1 = [c1 stat(k).Centroid(1)];
    c2 = [c2 stat(k).Centroid(2)];
end;

%trova gli angoli nord-est e sud-ovest
for k = 1 : numel(stat)
    ne = find((c1 - dxM) == (stat(gbM).Centroid(1) - dxM));
    sw = find((c2 - dxm) == (stat(gbm).Centroid(2) - dxm));
end;

%elimina i centroidi delle barre-guida
ne(find(gbM)) = [];
sw(find(gbm)) = [];

%l'angolo nord-ovest ha le stesse coordinate degli angoli nord-est e
%sud-ovest
nw = [stat(ne).Centroid(2) stat(sw).Centroid(2)];

m = [nw ne sw];

bw3 = ismember(labelmatrix(cc), m);

%coordinate dei punti
x = [stat(1).Centroid(1) stat(3).Centroid(1) stat(6).Centroid(1)];
y = [stat(1).Centroid(2) stat(3).Centroid(2) stat(6).Centroid(2)];

%calcolo dell'ultimo punto
xM = (x(2) + x(3)) / 2;
yM = (y(2) + y(3)) / 2;
xD = (2 * xM) - x(1); 
yD = (2 * yM) - y(1);

x = [x xD];
y = [y yD];

figure, imshow(im), hold on;

for k = 1 : numel(x)
    text(x(k), y(k), '**', 'Color', 'r');
end;
%rectangle('Position', x, y, 'EdgeColor', 'g');

hold off;




    