%% generate_marker
%genera un marker dalla sequenza di bit passata in input
%data -> sequenza di 83 bit

function out = generate_marker(data)

tic; 

% Embed 83 bits of data in the 2-D code marker
%   data: 1-by-83, composed of 0's and 1's
%   code marker: 11-by-11, composed of 0's and 1's
%                0 and 1 correspond to white and black dots respectively

code = [ ...
    1 0 2 2 2 2 2 2 2 0 1; ...
    0 0 2 2 2 2 2 2 2 0 0; ...
    2 2 2 2 2 2 2 2 2 0 1; ...
    2 2 2 2 2 2 2 2 2 0 1; ...
    2 2 2 2 2 2 2 2 2 0 1; ...
    2 2 2 2 2 2 2 2 2 0 1; ...
    2 2 2 2 2 2 2 2 2 0 1; ...
    2 2 2 2 2 2 2 2 2 0 1; ...
    2 2 2 2 2 2 2 2 2 0 1; ...
    0 0 2 2 2 0 0 0 0 0 0; ...
    1 0 2 2 2 0 1 1 1 1 1];

code(find(code == 2)) = data;

out = ones(13);
out(2:12,2:12) = ~code;

%visualizza il marker generato
%figure, imagesc(out), colormap gray;
%axis equal; axis off;
%title('Marker generato');

%visualizza la codifica in bit passata in input e il tempo di codifica
%disp(['Sequenza da codificare: ' num2str(data)]);
disp(['Codifica in ' num2str(toc) ' secondi']);
