%% bar_rotate
%ruota il marker rilevato in base alla posizione e all'angolo di
%orientamento delle barre-guida
%mk -> marker
%fi -> immagine "filled" del marker

function  outM  = bar_rotate(mk, fi)

%figure, imshow(mk), figure, imshow(fi);
%estrae le regioni che corrispondono alle barre guida del marker. In questo
%punto del programma vengono esclusi i falsi positivi, cio� quelle regioni
%che hanno le caratteristiche dei marker nello step precedente ma in realt�
%non hanno le barre guida
ccO = bwconncomp(~mk);

statO = regionprops(ccO, {'Area', 'Centroid', 'Orientation' ...
    'MajorAxisLength', 'MinorAxisLength', 'Image', ...
    'BoundingBox', 'Eccentricity'});

idxO = find([statO.MajorAxisLength] ./ [statO.MinorAxisLength] > 3 & ...
    [statO.MajorAxisLength] ./ [statO.MinorAxisLength] < 12 & ...
    [statO.Eccentricity] >= 0.97 & [statO.Eccentricity] <= 0.99 & ...
    [statO.Area] < 12000);


%se idxO � uguale a 0, i marker rilevati sono falsi positivi
if (numel(idxO) > 0)
    %seleziona solo le 2 barre guida
    mb = [statO.MajorAxisLength] == min([statO(idxO).MajorAxisLength]);
    mB = find([statO.MajorAxisLength] == max([statO(idxO).MajorAxisLength]));

    %riallinea il marker in base all'angolo di orientazione della barra
    %guida pi� grande. Il marker in questo modo � dritto in relazione alle
    %barre guida
    mkR = imrotate(mk, 90 - statO(mB).Orientation);
    fiR = imrotate(fi, 90 - statO(mB).Orientation);

    %figure, imshow(mkR);
    %estrae dal marker ruotato nuovamente la barra-guida pi� piccola
    ccR = bwconncomp(~mkR);
    
    statR = regionprops(ccR, {'Area', 'Centroid', 'Orientation' ...
    'MajorAxisLength', 'MinorAxisLength', 'Eccentricity'});

    idxR = [statR.MajorAxisLength] ./ [statR.MinorAxisLength] > 3 & ...
    [statR.MajorAxisLength] ./ [statR.MinorAxisLength] < 12 & ...
    [statR.Eccentricity] >= 0.97 & [statR.Eccentricity] <= 0.99 & ...
    [statR.Area] < 12000;

    mbR = [statR.MajorAxisLength] == min([statR(idxR).MajorAxisLength]);
    %salva il centroide della barra
    cR = statR(mbR).Centroid;
    
    
    %calcola dall'immage "filled" che rappresenta la regione in modo
    %omogeneo, il centroide che indica il centro del marker
    ccFI = bwconncomp(fiR);
    statFI = regionprops(ccFI, 'Centroid');
    cFI = statFI.Centroid;
    
    %figure, imshow(mkR), hold on;
    %text(cR(1), cR(2), '++', 'Color', 'y');
    %text(cFI(1), cFI(2), '++', 'Color', 'r');
    %hold off;
    
    %calcola in quale quadrante si trovano le barre-guida. In questo modo �
    %possibile ruotare il marker in modo che rispetti le caratteristiche
    %per la codifica
    %nord-est
    if(cR(2) > cFI(2) && cR(1) < cFI(1))
        angle = 90;
    end;
    %sud-est
    if(cR(2) > cFI(2) && cR(1) > cFI(1))
        angle = 0;
    end;
    %nord-ovest
    if(cR(2) < cFI(2) && cR(1) < cFI(1))
        angle = -180;
    end;
    %sud-ovest
    if(cR(2) < cFI(2) && cR(1) > cFI(1))
        angle = -90;
    end;
    mkR = imrotate(mkR, angle);
    fiR = imrotate(fiR, angle);
    
    
    %figure, imshow(mkR), figure, imshow(fiR);
    outM = mkR;
    outF = fiR;
    
    %richiama nuovamente la funzione "grid" per ricostruire la cornice
    %bianca attorno al marker
    outM = grid(outM, outF);
    
    %calcola la regione nell'immagine "filled" del marker
    ccB = bwconncomp(outF);
    
    statB = regionprops(ccB, 'BoundingBox');
    
    outM = imcrop(outM, statB.BoundingBox);
    
else 
    disp('Falso positivo');
    outM = [];    
end;

