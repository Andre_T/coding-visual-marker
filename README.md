# Localizzazione e codifica di marker visuali presenti in immagini #

## Contesto ##
I marker sono utilizzati in molte applicazioni per lo scambio di dati tramite la lettura del codice che riportano. Impiegati per la descrizione di oggetti, link per siti web, configurazioni di dispositivi.

## Requisiti ##
* MATLAB

## Funzionalità ##
Lo script prende in input un’immagine con uno o più marker, localizza l’area contenente i/il marker e ne estrae il contenuto. Ricostruisce il marker estratto posizionando in modo da poter essere letto e codificato correttamente. I marker localizzati e codificati da questo script prevedono un’area dati di 83 bit individuata da due barre e tre punti per l’allineamento.

![Schermata 2014-10-30 alle 11.19.53.png](https://bitbucket.org/repo/qnj8EE/images/3527011313-Schermata%202014-10-30%20alle%2011.19.53.png)

## Istruzioni ##
Lo script per eseguire la localizzazione e codifica dei marker è “marker_detection”. 
Specificando come input il pathname dell’immagine, la funzione restituisce in output il 
numero di marker localizzati, la presenza di falsi positivi e di marker, il tempo per la 
localizzazione e la codifica e la sequenza di 83 bit per ogni marker localizzato. Genera e 
visualizza un marker di confronto per ogni sequenza di 83 bit.


## Note ##
Le immagini in input devono essere RGB, poiché lo script le converte in immagini a livelli di grigio e ne esegue l’elaborazione.
Per maggiori informazioni vedi -> Relazione.pdf