%% grid
%seleziona nel marker trovato, la regione con l'area maggiore, 
%corrispondente all'intero marker e crea una cornine di pixel bianchi
%per costruire un marker con le caratteristiche ottimali per essere
%codificato
%bw -> immagine binaria della regione contenente il marker
%fi -> immagine "filled" della regione

function m = grid(bw, fi)


%porta il marker e la sua immagine "filled" alla stessa dimensione
bw = imresize(bw, size(fi), 'bilinear');


%costruisce la cornice sommando all'immagine binaria del marker
%il complemento della regione
m = bw .* fi;
m = m + ~fi;


%allarga le barre-guida applicando l'operazione di chiusura sul complemento
%del marker. Applicando l'operazione morfologica sul complemento del marker
%non vengono dilatati i pixel neri isolati introdotti dalla funzione "grid" 
mi = bwmorph(~m, 'close', 3);
m = ~mi;


%con l'operazione di chiusura elimina i pixel neri isolati e i bordi 
%seghettati del marker dovuti alla binarizzazione
m = bwmorph(m, 'close', 3);


%opzionale: mostra il marker ottenuto
%figure, imshow(m);
%imwrite(m, 'prova2.png');

