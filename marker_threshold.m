%% marker_threshold
%stima automaticamente la soglia ideale per la binarizzazione
%im -> immagine su cui viene applicato l'oggetto per l'autothresholding

function [out, th] = marker_threshold(im)

%crea un Autothresholder object che verr� utilizzato per
%identificare una soglia automatica locale
bin = vision.Autothresholder( ...
        'Operator', '>', ...
        'ThresholdOutputPort', true, ...
        'InputRangeSource', 'Auto');
        %'InputRange', threshold);
    
%applica l'oggetto 'bin'all'immagine restituendo in output
%un'immagine binarizzata e una soglia. L'immagine binarizzata 
%non verr� utilizzata
[out, th] = step(bin, im);

