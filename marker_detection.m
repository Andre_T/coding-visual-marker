%%   marker_detection    
%rileva i marker dall'immagine passata in input con il suo pathname in 
%base a caratteristiche comuni a tutti i marker nelle foto.

%pathImage -> percorso dell'immagine in cui rilevare la presenza di
%eventuali marker

function marker_detection(pathImage)

close all;
%variabile inizio tempo
tic;

im = imread(pathImage);

%richiama la funzione di binarazzione delle immagini
[bw, ig] = marker_bin(im); 

%seleziona le regione che corrispondono ai marker. La selezione avviene in
%base a caratteristiche comuni a tutti i marker
cc = bwconncomp(bw);
stat = regionprops(cc, ig, {'Area', 'MajorAxisLength', 'MinorAxisLength', ...
    'EulerNumber', 'MeanIntensity'});
idx = find(([stat.MajorAxisLength]./[stat.MinorAxisLength]) > 0.9 ...
    & ([stat.MajorAxisLength]./[stat.MinorAxisLength]) < 1.6 ...
    & [stat.MeanIntensity] > 170 & [stat.Area] > 9000 & ...
    [stat.Area] < 700000 & [stat.EulerNumber] > - 200 & [stat.EulerNumber] < -1);
  
%immagine binaria con le sole regioni che corrispondono ai marker
bwR = ismember(labelmatrix(cc), idx);
 

%display della segmentazione
figure, 
subplot(2, 2, 1);
imshow(im);
subplot(2, 2, 2);
imshow(ig);
subplot(2, 2, 3);
imshow(bw);
subplot(2, 2, 4);
imshow(bwR);

%seleziona le regioni dai marker dall'immagine binaria ottenuta in
%precendenza
ccM = bwconncomp(bwR);
stats = regionprops(ccM, ig, {'Area', 'Image', ...
    'Centroid', 'FilledImage', 'BoundingBox'});

%display dell'immagine originale
figure, imshow(im), hold on;

%se sono presenti marker nella foto
if(numel(stats) > 0)
disp(['Marker trovati: ' num2str(numel(stats))]);

%struttura per la memorizzazione dei marker generati utilizzando la
%codifica di quelli rilevati nelle foto
ind = [];
mg = struct;
    
    %per ogni marker estrae la porzione d'immagine in cui � contenuto il
    %marker
    for o = 1 : numel(stats)
        mk = imadjust(imcrop(ig, stats(o).BoundingBox));
        %migliora la porzione d'immagine che contiene il marker
        mk = imsharpen(mk, 'Threshold', 0.4);
        %soglia per la binarizzazione
        thG= graythresh(mk);
        s = grid(im2bw(mk, thG), stats(o).FilledImage);
        out = bar_rotate(s, stats(o).FilledImage);
        
        %se la variabile "out" � vuota, il marker passato alla "funzione"
        %non aveva le barre-guida, quindi � un falso positivo
        if(isempty(out))
            %evidenzia i marker rilevati nell'immagine originale
            rectangle('Position', stats(o).BoundingBox, 'EdgeColor', 'r');
        else
            p = sampler(out, thG);
            code = decode_cmv(p);
            disp(['Sequenza bit del marker: ' num2str(code)]);
            %memorizza i marker generati con le sequenze di bit
            %decodificate dai marker rilevati
            mg(o).Marker = generate_marker(code);
            %elimina gli indici dei falsi positivi
            ind = [ind, o];
            rectangle('Position', stats(o).BoundingBox, 'EdgeColor', 'g');
        end;
    end;
    hold off;
    
    %visualizza i marker generati
    for l = 1 : numel(ind)
        figure, imagesc(mg(ind(l)).Marker), colormap gray;
    end;
    disp(['Tempo di esecuzione: ' num2str(toc) ' secondi']);

else
    disp('Nessun marker trovato.');
    disp(['Tempo di esecuzione: ' num2str(toc) ' secondi']);
end;










