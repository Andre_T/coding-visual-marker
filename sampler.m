%% sampler
%ricampiona il marker localizzato partendo da una dimensione di 169x169 del
%marker originale. Il campionamento avviene in base alla media locale con
%una finestra di 13 pixel. La media in ogni finestra � poi confrontanta con
%il threshold globale dell'immagine per la binarizzazione locale di ogni
%finestra.

%m -> marker rilevato
%thG -> soglia calcolata con la funzione graythresh


function p = sampler(m, thG)


m = imresize(m, [169 169]);


%dimensione della finestra per il campionamento locale
xs = 1;
ys = 1;
dy = 13;
dx = 13;
o = true([1 13]);

%figure, imshow(m), hold on;

%campiona il marker con finestre di 13x13 pixel su cui calcola la media
%dell'intensit�. Crea una matrice composta dalle medie dell'intensit� di
%ogni sottofinestra
for y = 0 : 12
    yi = ys + dy * y; 
    patches = [];
    for x = 0 : 12
        xi = xs + dx * x;
        patch = m(yi : yi + dy - 1, xi : xi + dx - 1);
        patches = [patches, mean2(patch)];
        %rectangle('Position', [yi, xi, dy, dx], 'EdgeColor', 'g');
    end;
    o = [o; patches];
end;
%hold off;
o(1, :) = [];

%binarizzazione locale con soglia singola
mk = [];
for i = 1 : 13
    for j = 1 : 13
        if(o(i, j) > thG)
           mk(i, j) = 1;
        else
           mk(i, j) = 0;
        end;
    end;
end;

p = mk;

