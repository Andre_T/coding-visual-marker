%% decode_cmv
%Decodifica il marker passato come immagine binaria, 
%in una sequenza di 83 bit
%m -> marker 13x13 da codificare


function out = decode_cmv(m)


%variabile inizio tempo di codifica
tic;


%maschera per l'estrazione della sequenza di bit
code = [ ...
    1 0 2 2 2 2 2 2 2 0 1; ...
    0 0 2 2 2 2 2 2 2 0 0; ...
    2 2 2 2 2 2 2 2 2 0 1; ...
    2 2 2 2 2 2 2 2 2 0 1; ...
    2 2 2 2 2 2 2 2 2 0 1; ...
    2 2 2 2 2 2 2 2 2 0 1; ...
    2 2 2 2 2 2 2 2 2 0 1; ...
    2 2 2 2 2 2 2 2 2 0 1; ...
    2 2 2 2 2 2 2 2 2 0 1; ...
    0 0 2 2 2 0 0 0 0 0 0; ...
    1 0 2 2 2 0 1 1 1 1 1];


%seleziona solo la regione in cui � registrato il codice, escludendo le 
%barre-guida
mk = m(2:12,2:12);
bit = [];


%estrae il codice 
for i = 1:size(code, 1)
    for j = 1:size(code, 1)
        if (code(j,i) == 2)
            bit = [bit mk(j,i)];
        end
    end
end


%restituisce la codifica del marker in bit
out = ~bit;

%opzionale: 
%visualizza il marker passato in input
%figure, imagesc(m), colormap gray;
%axis equal; axis off;
%title('Marker in input');


%visualizza il tempo di decodifica della sequenza di 83 bit
disp(['Decodifica in ' num2str(toc) ' secondi']);
